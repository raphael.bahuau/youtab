chrome.runtime.onInstalled.addListener(function () {
    // Replace all rules ...
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
        // With a new rule ...
        chrome.declarativeContent.onPageChanged.addRules([
            {
                // That fires when a page's URL is equals to...
                conditions: [
                    new chrome.declarativeContent.PageStateMatcher({
                        // pageUrl: { hostEquals: 'www.youtube.com' } => onglets
                        // pageUrl: { hostEquals: 'www.youtube.com', pathEquals: '/feed/subscriptions' } => autofill channelinput
                    }),
                    // new chrome.declarativeContent.PageStateMatcher({
                    //     pageUrl: { hostEquals: 'www.youtube.com', pathPrefix: '/user/' } => autofill channelinput
                    // })
                    // https://www.youtube.com/feed/channels => onglets
                ],
                // And shows the extension's page action.
                actions: [new chrome.declarativeContent.ShowPageAction()]
            }
        ])
    })
})

