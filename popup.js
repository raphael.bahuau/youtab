// $(document).ready(() => {})

// chrome.storage.sync.clear()

// Get the categories in storage then setup the popup
chrome.storage.sync.get('categories', result => {
    const categories = initStorage(result)

    buildHtml(categories)

    bindFunctions(categories)

    // "Other" category
    $('#toggle-switch-Other').click(e => toggleOtherVideos(categories, e))

    for (let category of categories) {
        if (category.checked) {
            console.log(category.name)
            $(`#toggle-switch-${category.name}`).click()
        }
    }

    // Download configuration
    $('.fa-file-download').click(e => exportJson(categories, e))

    // Import configuration, need to open extension console with right click "inspect"
    $('#file-upload').change(e => importJson(e))

    // Reload button, just update categorie to trigger videos toggler
    $('.fa-redo-alt').click(e => reload(categories, e))
})

//--- STORAGE ----------------------------------------------------------------//

// Return a new category
function newCategorie(name) {
    return { name: name, checked: false, channels: [] }
}

// Return a category by name in categories array
function getCategory(categories, name) {
    for (category of categories) {
        // Compare to lower case to prevent duplicate when create new category
        if (category.name.toLowerCase() === name.toLowerCase()) {
            return category
        }
    }
    return undefined
}

// Return the categories in storage, create it the first time
function initStorage(result) {
    let categories = result.categories
    // If there is no categories, create and store defaults
    if (categories === undefined) {
        categories = []
        const names = ['Music', 'Entertainment', 'Game', 'Education', 'Other']
        for (let name of names) {
            category = newCategorie(name)
            categories.push(category)
        }
        saveStorage(categories)
    }
    // Sort categories and channels by name
    categories.sort((a, b) => 1 - 2*(a.name > b.name))
    for (category of categories) {
        category.channels.sort()
    }
    return categories
}

// Save categories in chrome storage
function saveStorage(categories) {
    chrome.storage.sync.set({ 'categories': categories })
}

//--- HTML -------------------------------------------------------------------//

// Build the html popup
function buildHtml(categories) {
    accordion = document.querySelector('.accordion')
    for (let category of categories) {
        if (category.name !== 'Other') {
            template = getCategoryHTML(category)
            accordion.innerHTML = template + accordion.innerHTML
        }
    }
}

// Return a category collapse card template
//  https://getbootstrap.com/docs/4.1/components/collapse
// hided='hided' to dynamically hide the card when whe want to remove it
function getCategoryHTML(category, hided='') {
    const channelsTemplate = getChannelsHTML(category.channels)
    const categoryTemplate = `
        <div class="card user-category" data-category="${category.name}">
            <div class="card-header ${hided}" id="heading-${category.name}">
                <div class="toggle-switch">
                    <input type="checkbox" id="toggle-switch-${category.name}">
                    <label for="toggle-switch-${category.name}"></label>
                </div>
                <h5 data-toggle="collapse" aria-expanded="true"
                    data-target="#collapse-${category.name}"
                    aria-controls="collapse-${category.name}">
                    ${category.name}
                    <span class="badge badge-pill badge-danger">
                        ${category.channels.length}
                    </span>
                </h5>
                <i class="fas fa-times"></i>
            </div>
            <div id="collapse-${category.name}" class="collapse"
                aria-labelledby="heading-${category.name}">
                <ul class="card-body">
                    <li class="chip chip-input input-channel">
                        <i class="toggle fas fa-plus"></i>
                        <input type="text" placeholder="channel name" autofocus>
                    </li>
                    ${channelsTemplate}
                </ul>
            </div>
        </div>`
    return categoryTemplate
}

// Return all channels chip templates
function getChannelsHTML(channels) {
    let channelsTemplate = ''
    for (channel of channels) {
        channelsTemplate += getChannelHTML(channel)
    }
    return channelsTemplate
}

// Return a channel chip template
function getChannelHTML(channel) {
    return `<li class="chip">${channel}<i class="fas fa-times"></i></li>`
}

//--- FUNCTIONS --------------------------------------------------------------//

// Bind all functions on current HTML, called when adding category or channel
function bindFunctions(categories) {
    // First call .off() to unbind, prevent double binding

    // Toggle category input & files icons when click on the (+) category chip
    $('.input-category .toggle').off('click').click(e => toggleChip(e))

    // Toggle videos when toogle category checkboxes
    $('.user-category .toggle-switch input').off('change').change(e => toggleVideos(categories, e))

    // Toggle the channel inputs when click on the (+) channel chip
    $('.input-channel .toggle').off('click').click(e => toggleInput(e))
    
    // Add channel when press 'enter' after fill a channel input
    $('.chip-input.input-channel input').off('keypress').keypress(e => addChannel(categories, e))

    // Remove channel when click on channel chip cross
    $('.chip .fa-times').off('click').click(e => removeChannel(categories, e))

    // Add category when press 'enter' after fill the category input
    $('.chip-input.input-category input').off('keypress').keypress(e => addCategory(categories, e))

    // Remove category when click on category card cross
    $('.card-header .fa-times').off('click').click(e => removeCategory(categories, e))
}

// Toggle input
function toggleInput(e) {
    const icon = e.currentTarget
    const li = icon.parentElement
    // Open input and focus it
    if (li.classList.toggle('opened')) {
        const input = icon.nextElementSibling
        input.focus()
    }
    // Change icon from (+) to (<)
    icon.classList.toggle('fa-chevron-left')
    icon.classList.toggle('fa-plus')
}

// Toggle category input & files icon
function toggleChip(e) {
    toggleInput(e)
    $('.chip-tohide').toggleClass('hided')
}

// Toggle videos on Youtube page
function toggleVideos(categories, e) {
    const checkbox = e.currentTarget
    const card = checkbox.parentElement.parentElement.parentElement
    // Save checkbox state in chrome storage
    category = getCategory(categories, card.dataset.category)
    category.checked = checkbox.checked
    console.log(categories)
    saveStorage(categories)
    // When storage is updated, the listener in page-script.js toggle videos
    disableCheckbox(categories)
}

// Toggle other videos on Youtube page
function toggleOtherVideos(categories, e) {
    console.log(e)
    const checkbox = e.currentTarget
    category = getCategory(categories, 'Other')
    category.checked = checkbox.checked
    saveStorage(categories)
    disableCheckbox(categories)
}

// Disable checkbox to only keep Other or user category active
function disableCheckbox(categories) {
    $('.user-category .toggle-switch input').prop('disabled', false)
    $('#toggle-switch-Other').prop('disabled', false)
    for (let category of categories) {
        if (category.checked) {
            const isOther = (category.name === 'Other')
            $('.user-category .toggle-switch input').prop('disabled', isOther)
            $('#toggle-switch-Other').prop('disabled', !isOther)
        }
    }
}

// Get an input value, return undefined if key pressed isn't 'enter'
// Ignore empty string
function getInputValue(e) {
    // Exit function if the key pressed isn't 'enter'
    const keycode = (e.keyCode ? e.keyCode : e.which)
    if (keycode !== 13) {
        return
    }
    // Get the input value
    const input = e.currentTarget
    const value = input.value.trim()
    return value
}

// Add channel to a category (finded by the 'e' param)
function addChannel(categories, e) {
    // Get the new channel name
    const value = getInputValue(e)
    if (!value) {
        return
    }
    
    const input = e.currentTarget
    const ul = input.parentElement.parentElement
    const card = ul.parentElement.parentElement
    const category = getCategory(categories, card.dataset.category)

    // Prevent same name of an other channel in this category
    if (category.channels.includes(value.toLowerCase())) {
        alert('This channel is already in this category')
        return
    }

    // Reset the input value in HTML
    input.value = ''

    // Add new channel name chip in HTML
    const template = getChannelHTML(value)
    ul.innerHTML = ul.innerHTML.replace('</li>', '</li>' + template)

    // Save new channel in storage
    category.channels.push(value)
    saveStorage(categories)
    // This fire the listener in page-script.js to toggle videos

    // Update the badge counter in HTML
    updateBadgeHTML(category, ul)

    // Rebind functions
    bindFunctions(categories)

    // Finally close input
    setTimeout(() => {
        const li = ul.firstElementChild
        li.classList.remove('opened')
        const icon = li.querySelector('i')
        icon.classList.remove('fa-chevron-left')
        icon.classList.add('fa-plus')
    }, 0)
}

// Remove channel from category (finded by the 'e' param)
function removeChannel(categories, e) {
    const icon = e.currentTarget

    // Add 'hided' class to chip, then remove after transition
    const li = icon.parentElement
    const ul = li.parentElement
    li.classList.add('hided')
    li.addEventListener('transitionend', () => ul.removeChild(li), { once:true })

    // Update storage (who fire listener in page-script.js to toggle videos)
    const channel = li.textContent
    const card = ul.parentElement.parentElement
    const category = getCategory(categories, card.dataset.category)
    const index = category.channels.findIndex(v => v === channel)
    category.channels.splice(index, 1)
    saveStorage(categories)

    // Update the badge counter in HTML
    updateBadgeHTML(category, ul)
}

// Update the number of channels displayed on category card
function updateBadgeHTML(category, ul) {
    const cardHeader = ul.parentElement.previousElementSibling
    const badge = cardHeader.querySelector('.badge')
    badge.innerHTML = category.channels.length
}

// Add category
function addCategory(categories, e) {
    // Get the new category name, return if key pressed isn't 'enter'
    const value = getInputValue(e)
    if (!value) {
        return
    }
    
    // Prevent same name of an other category
    if (getCategory(categories, value) || value.toLowerCase() === 'other') {
        alert('This category already exist')
        return
    }

    // Reset the input value in HTML
    const input = e.currentTarget
    input.value = ''

    // Create the new category
    const category = newCategorie(value)

    // Add category card in HTML
    const accordion = document.querySelector('.accordion')
    const template = getCategoryHTML(category, 'hided')
    const s = `<div class="card accordion-footer`
    accordion.innerHTML = accordion.innerHTML.replace(s, template + s)

    // Update storage (who fire listener in page-script.js to toggle videos)
    categories.push(category)
    saveStorage(categories)

    // Rebind functions
    bindFunctions(categories)

    // Finally close input & toggle files icons
    setTimeout(() => {
        const li = document.querySelector('.chip-input.input-category')
        li.classList.remove('opened')
        const icon = li.querySelector('i')
        icon.classList.remove('fa-chevron-left')
        icon.classList.add('fa-plus')

        const footer = document.querySelector('.accordion-footer')
        const card = footer.previousElementSibling
        const cardHeader = card.querySelector('.card-header')
        cardHeader.classList.remove('hided')

        $('.chip-tohide').toggleClass('hided')
    }, 0)
}

// Remove category
function removeCategory(categories, e) {
    const icon = e.currentTarget
    const cardHeader = icon.parentElement
    const card = cardHeader.parentElement

    const category = getCategory(categories, card.dataset.category)

    // Alert to confirm deletion
    if (!confirm(`Are you sure you want to delete the ${category.name} category and all its channels ?`)) {
        return
    }

    // Add 'hided' class to card, then remove after transition
    cardHeader.classList.add('hided')
    $('#collapse-' + category.name).collapse('hide')

    const accordion = document.querySelector('.accordion')
    cardHeader.addEventListener('transitionend', () => accordion.removeChild(card), { once:true })

    // Update storage
    const index = categories.findIndex(c => c.name === category.name)
    categories.splice(index, 1)
    saveStorage(categories)
}

// Export the configuration
function exportJson(categories, e) {
    const a = e.currentTarget.parentElement
    a.href = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(categories))
}

// Import the configuration
function importJson(e) {    
    // Reader to read files from input
    const reader = new FileReader();

    // Function called when readAs function are called
    reader.onload = () => {
        try {
            const categories = JSON.parse(reader.result)
            saveStorage(categories)
        } catch (e) {
            alert('Something went wrong during the file reading, please check your youtab.json file')
        }
    }
  
    // Read the file
    reader.readAsText(e.target.files[0]);
    window.close()
}

// Reload the video toggler
function reload(categories, e) {
    category = getCategory(categories, 'Other')
    category.channels[0] = Date()
    saveStorage(categories)
}