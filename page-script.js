(function () {
    // Storage listener to detect when categories are checked to toggle videos
    chrome.storage.onChanged.addListener((changes, namespace) => {
        const storageChange = changes.categories
        if (storageChange && namespace === 'sync') {
            const categories = storageChange.newValue
            toggle(categories)
        }
    })

    // First toggle when Youtube is loaded
    chrome.storage.sync.get('categories', result => {
        const categories = result.categories
        if (categories) {
            toggle(categories)
        }
    })
})()


// Show or hide videos according to categories selected in extension
function toggle(categories) {
    // Get channel titles to keep visible (or to show)
    let visibles = []
    // If the number of categories selected is 0, show all channels
    let numberChecked = 0
    // Determine if it's category other, to hide all channels in categories
    let isOther = false
    for (category of categories) {
        if (category.name === 'Other' && category.checked) {
            isOther = true
        }
    }
    const hiddenClass = (isOther) ? 'youtab-visible' : 'youtab-hidden'
    const visibleClass = (isOther) ? 'youtab-hidden' : 'youtab-visible'

    for (let category of categories) {
        if (category.checked || isOther) {
            visibles = visibles.concat(category.channels)
            numberChecked++
        }
    }

    // Get videos elements
    const cssSelectorGrid = 'ytd-section-list-renderer'
    const eGrid = document.querySelector(cssSelectorGrid)

    const cssSelectorVideo = 'ytd-grid-video-renderer'
    const eVideos = eGrid.querySelectorAll(cssSelectorVideo)

    const cssSelectorChannelName = '#channel-name #text'

    // Show or hide videos
    for (let eVideo of eVideos) {
        // Get the channel title of the video and toogle her
        const title = eVideo.querySelector(cssSelectorChannelName).textContent

        // If need to show
        if (visibles.includes(title) || numberChecked === 0) {
            eVideo.classList.remove(hiddenClass)
            // Remove all youtab class if there is 0 categories selected
            if (numberChecked === 0) {
                eVideo.classList.remove(visibleClass)
            } else {
                eVideo.classList.add(visibleClass)
            }
        }
        // Else hide
        else {
            eVideo.classList.remove(visibleClass)
            eVideo.classList.add(hiddenClass)
        }
        // Bind the add channel function if one category is selected
        if (numberChecked === 1) {
            eVideo.classList.add('youtab-edit')
            eVideo.onclick = e => toggleChannel(e, categories, title)
        } else {
            eVideo.classList.remove('youtab-edit')
            eVideo.onclick = undefined
        }
    }
}

// Add or Remove channel in categories when click on button on videos
function toggleChannel(e, categories, title) {
    const eVideo = e.target
    
    // Check if clicked on the youtab icon on video
    const bbox = {
        x1: eVideo.offsetLeft + 4,
        y1: eVideo.offsetTop + 4,
        x2: eVideo.offsetLeft + 32,
        y2: eVideo.offsetTop + 32
    }
    if (bbox.x1 > e.pageX || bbox.y1 > e.pageY || bbox.x2 < e.pageX || bbox.y2 < e.pageY) {
        return
    }
    
    // Add or remove channel from selected categories
    for (let category of categories) {
        if (category.checked) {
            if (eVideo.classList.contains('youtab-hidden')) {
                eVideo.classList.remove('youtab-hidden')
                eVideo.classList.add('youtab-visible')
                category.channels.push(title)
            } else {
                eVideo.classList.remove('youtab-visible')
                eVideo.classList.add('youtab-hidden')
                category.channels.splice(category.channels.indexOf(title), 1);
            }
        }
    }
    
    // Finally store new configuration
    saveStorage(categories)
}

// Save categories in chrome storage
function saveStorage(categories) {
    chrome.storage.sync.set({ 'categories': categories })
}